myTupple = (34,)
print(type(myTupple))

myTupple = (34,45)
print(type(myTupple))

#myTupple[0] = 78#generates an error
print(myTupple)

myTupple = myTupple + ("Niit","Jadavpur","Kolkata",23.786,True)
print(myTupple)

print(myTupple[3])

print(myTupple[2:5])

print(myTupple[::-1])


print(len(myTupple))

print(myTupple.count(34))
print(myTupple.index(23.786))


